import DigitalClock from './components/digitalClock/DigitalClock';
import './App.scss';
import Countdown from './components/countdown/Countdown';
import Stopwatch from './components/stopwatch/Stopwatch';


function App() {
  return (
    <div className="app">

      <DigitalClock />
      <Countdown />
      <Stopwatch />
    </div>
  );
}

export default App;
